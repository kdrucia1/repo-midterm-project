//Tej Jolly & Kevin Druciak
//tjolly4 & ______
// __Add your name and JHED above__
// ppm_io.c
// 601.220, Spring 2019
// Starter code for midterm project - feel free to edit/add to this file

#include <assert.h>
#include <stdlib.h>
#include "ppm_io.h"



/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */
Image * read_ppm(FILE *fp) {

  // check that fp is not NULL
  assert(fp);

  Image* im = malloc(sizeof(Image));
  if (!im) {
    fprintf(stderr, "Uh oh. Image allocation failed!\n");
  }
  
  int colors = 0;
  int temp1 = 0;
  int temp2 = 0;
  char temp[9];

  
  while (colors == 0) {
    fgets(temp, sizeof(temp), fp);
    if (sscanf(temp, "%d %d", &temp1, &temp2) == 2) {
      im->cols = temp1;
      im->rows = temp2;
    } else if (sscanf(temp, "%d %d", &temp1, &temp2) == 1) {
      colors = temp1;
    }
  }

  printf("Width: %d, Rows: %d\n", im->cols, im->rows);

  if (colors != 255) {
    fprintf(stderr, "Invalid PPM file (colors should equal 255)");
  }

  Pixel* pix = malloc(sizeof(Pixel) * im->rows * im->cols);
  if (!pix) {
    fprintf(stderr, "Uh oh. Pixel array allocation failed!\n");
    free(im);
  }
  
  int read = fread(pix, sizeof(Pixel), im->rows * im->cols, fp); 

  /*
  int i = 0;
  while(fread(temp, 1, 1, fp) == 1) {
    switch (i%3) {
    case 0: (pix+i)->r = *temp; i++; break;
    case 1: (pix+i)->g = *temp; i++; break;
    case 2: (pix+i)->b = *temp; i++; break;
    }
  }
  */
  if (read != im->rows*im->cols) {
    // print error
  }
  im->data = pix;

  fclose(fp);

  return im;
  
}


/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE* fp, const Image* im) {

  // check that fp is not NULL
  assert(fp); 

  // write PPM file header, in the following format
  // P6
  // cols rows
  // 255
  fprintf(fp, "P6\n%d %d\n255\n", im->cols, im->rows);

  // now write the pixel array
  int num_pixels_written = fwrite(im->data, sizeof(Pixel), im->cols * im->rows, fp);

  if (num_pixels_written != im->cols * im->rows) {
    fprintf(stderr, "Uh oh. Pixel data failed to write properly!\n");
  }

  return num_pixels_written;
}

