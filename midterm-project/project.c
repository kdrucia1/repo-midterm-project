#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ppm_io.h"

int main(int argc, char* argv[]) {

  if (argc < 3) {
    fprintf(stderr, "Please enter exactly one input and one output file");
    return 1;
  }

  //Takes second argument in command line and opens for reading in binary
  FILE* in_file = fopen(argv[1], "rb");
  if (!in_file) {
    fprintf(stderr, "Input file failed to open!\n");
    return 1;
  }

  // Chose operation to perform based on entry in command line
  if (!strcmp(argv[3], "copy")) {
    Image* im = read_ppm(in_file);
    if (!im) {
      printf("Failure");
    }

    FILE* out_file = fopen(argv[2], "wb");
    int num_pixels_written = write_ppm(out_file, im);
    printf("pixels written: %d\n", num_pixels_written);
  }
}
